#!/usr/bin/env python
# coding: utf-8

#vers
#inputfile=".csv" #important
workdir="db"


#imports
import sys,re,time,os
import codecs

import redis
R = redis.Redis(host='localhost',port=6379,db=0)

class tick():
	def __init__(self,done,loop=1000):
		self.loop=loop
		self.sec=self.loop
		self.done=done
		self.min=0
		
	def tick(self):
		self.sec=self.sec-1
		if self.sec<1:
			self.min=self.min+1
			self.sec=self.loop
			self.done(self.min,self.loop)
		
def deal(inputf):
	def done(min,sec):
		print min,sec
		P.execute()
		#P=R.pipeline()
	t=tick(done,10000)
	fin=codecs.open("db\\%s" %inputf,'r','gbk')
	ferr=codecs.open("dberror\\%s" % inputf,'a','utf-8')
	P=R.pipeline()

	while 1:
		t.tick()
		line = fin.readline()
		if not line:
			P.execute()
			break
		L=line.strip().split('----')
		if len(L)!=2:
			ferr.write("%s" % line)
		else:
			P.sadd(L[0],L[1])
	fin.close()
	ferr.close()
	#P.execute()


if __name__ == '__main__':
	for F in os.listdir(workdir):
		#if f.endswith(inputfile):
		print "Dealing with %s" % F
		deal(F)
		time.sleep(60*3)
	print "Finished!"
