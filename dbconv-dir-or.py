#!/usr/bin/env python 
# coding: utf-8

#vers
inputfile=".csv" #important
workdir="."
outputfile="ID.csv"
regp="(.*),,.*,ID,(\d*x{0,1}),"


#imports
import sys,re,time,os
import codecs


def deal(inputf):
	fin=codecs.open(inputf,'r','utf-8')
	fout=codecs.open(''.join(('out_',inputf)),'a','utf-8')
	ferr=codecs.open("error.txt",'a','utf-8')
	
	while 1:
		lines = fin.readlines(100000)
		if not lines:
			break
		for line in lines:
			rx1=re.search(regp,line,re.I)
			if rx1:
				fout.write("%s,%s\n" % (rx1.group(1),rx1.group(2))) #important for format and db name!
			else:
				ferr.write("%s" % line)
	
	fin.close()
	fout.close()
	ferr.close()
	

if __name__ == '__main__':
	for f in os.listdir(workdir):
		if f.endswith(inputfile):
			print "Dealing with %s" % f
			deal(f)
	print "Finished!"