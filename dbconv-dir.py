#!/usr/bin/env python
# coding: utf-8

#vers
#inputfile=".csv" #important
workdir="db"
maxthreads=2

#imports
import sys,re,time,os
import codecs
import pymysql
from multiprocessing import Pool,freeze_support

class tick():
	def __init__(self,done,loop=1000):
		self.loop=loop
		self.sec=self.loop
		self.done=done
		self.min=0

	def tick(self):
		self.sec=self.sec-1
		if self.sec<1:
			self.min=self.min+1
			self.sec=self.loop
			self.done(self.min,self.loop)

def deal(inputf):
	def done(min,sec):
		print min,sec
		conn.commit()

	print "Dealing with %s" % inputf
	tk=tick(done,10000)
	fin=codecs.open("db\\%s" %inputf,'r','gbk')
	ferr=codecs.open("dberror\\%s" % inputf,'a','utf-8')
	conn=pymysql.connect(host='127.0.0.1', port=3306, user='root', passwd='111111', db='db', charset='utf8')
	P=conn.cursor()

	while 1:
		tk.tick()
		try:
			line = fin.readline()
		except Exception as e:
			print 'Error:',e
			ferr.write("%s" % line)
			fin.next()
			continue
		if not line:
			break
		L=line.strip().split('----')
		if len(L)!=2:
			ferr.write("%s" % line)
		else:
			try:
				t=(None,None,L[0],L[1],None,'163',)
				P.execute("INSERT INTO db163 (ID,USERNAME,EMAIL,PASSWORD,SALT,SITE) VALUES (%s,%s,%s,%s,%s,%s)", t)
			except Exception as e:
				print 'Error:',e
				ferr.write("%s" % line)

	fin.close()
	ferr.close()
	conn.commit()
	P.close()
	conn.close()


if __name__ == '__main__':
	"""
	filepaths=list()
	for root, _, files in os.walk(folderpath):
		for name in files:
		  filepaths.append(os.path.join(root,name))
	"""
	freeze_support()
	pool=Pool(maxthreads)
	filepaths=os.listdir(workdir)
	res=pool.map(deal,filepaths)
	print "Finished!"
